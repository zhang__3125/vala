%global api_ver 0.56
%global priority 90
%global vala_binaries vala valac vala-gen-introspect vapigen
%global vala_manpages valac vala-gen-introspect vapigen

Name:           vala
Version:        0.56.3
Release:        3
Summary:        Compiler Using the GObject Type System
License:        LGPLv2+ and BSD
URL:            https://wiki.gnome.org/Projects/Vala
Source0:        https://download.gnome.org/sources/vala/0.56/vala-%{version}.tar.xz

BuildRequires:  flex bison glib2-devel gobject-introspection-devel
BuildRequires:  graphviz-devel libxslt dbus-x11 make
BuildRequires:  dbus-x11 chrpath
Requires(pre):  %{_sbindir}/alternatives
Requires:       vala-devel = %{version}-%{release} gobject-introspection-devel
Requires:       %{name} = %{version}-%{release}
Provides:       vala(api) = %{api_ver}
Obsoletes:      vala-tools < 0.34.0
Conflicts:      vala-tools < 0.34.0
Provides:       vala-tools = %{version}-%{release}
Provides:       valadoc = %{version}-%{release}
Obsoletes:      vala-tools < %{version}-%{release}
Conflicts:      vala-tools < %{version}-%{release}

%description
Compiler Using the GObject Type System.

%package    devel
Summary:    Library and and head file for vala compiler
Provides:   vala-devel = %{version}-%{release}
Obsoletes:  vala-devel < 0.43

%description devel
Library and and head file for vala compiler.

%package    help
Summary:    Help documents for vala
Requires:   %{name} = %{version}-%{release} devhelp

%description  help
Help documents for vala.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
sed -i 's|/lib /usr/lib|/lib /usr/lib /lib64 /usr/lib64|' libtool
make %{?_smp_mflags}

%install
%make_install
chrpath -d %{buildroot}%{_bindir}/vala
chrpath -d %{buildroot}%{_bindir}/vala-0.56
chrpath -d %{buildroot}%{_bindir}/valac
chrpath -d %{buildroot}%{_bindir}/valac-0.56
chrpath -d %{buildroot}%{_bindir}/valadoc
chrpath -d %{buildroot}%{_bindir}/valadoc-0.56
chrpath -d %{buildroot}%{_bindir}/vapigen
chrpath -d %{buildroot}%{_bindir}/vapigen-0.56
chrpath -d %{buildroot}%{_libdir}/vala-%{api_ver}/*.so
chrpath -d %{buildroot}%{_libdir}/*.so.*

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}-%{api_ver}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

mv %{buildroot}%{_bindir}/vala-gen-introspect-%{api_ver}{,-`uname -m`}
echo -e '#!/bin/sh\nexec %{_bindir}/vala-gen-introspect-%{api_ver}-`uname -m` "$@"' > \
  %{buildroot}%{_bindir}/vala-gen-introspect-%{api_ver}
  chmod +x %{buildroot}%{_bindir}/vala-gen-introspect-%{api_ver}

%delete_la_and_a

%check
make check

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license COPYING
%{_bindir}/vala
%{_bindir}/vala-%{api_ver}
%{_bindir}/valac
%{_bindir}/valac-%{api_ver}
%{_bindir}/vala-gen-introspect
%{_bindir}/vala-gen-introspect-%{api_ver}*
%{_bindir}/vapigen
%{_bindir}/vapigen-%{api_ver}
%{_libdir}/pkgconfig/vapigen*.pc
%{_libdir}/vala-%{api_ver}/
%{_datadir}/aclocal/vala.m4
%{_datadir}/aclocal/vapigen.m4
%{_datadir}/vala/
%{_datadir}/vala-%{api_ver}/
%{_bindir}/valadoc
%{_bindir}/valadoc-%{api_ver}
%{_libdir}/libvaladoc-%{api_ver}.so.0*
%{_libdir}/valadoc-%{api_ver}/
%{_datadir}/valadoc-%{api_ver}/
%config(noreplace) /etc/ld.so.conf.d/*

%files devel
%license COPYING
%{_libdir}/libvala-%{api_ver}.so.*
%{_includedir}/vala-%{api_ver}
%{_libdir}/libvala-%{api_ver}.so
%{_libdir}/pkgconfig/libvala-%{api_ver}.pc
%{_includedir}/valadoc-%{api_ver}/
%{_libdir}/libvaladoc-%{api_ver}.so
%{_libdir}/pkgconfig/valadoc-%{api_ver}.pc

%files help
%{_mandir}/man1/valac.1*
%{_mandir}/man1/valac-%{api_ver}.1*
%{_mandir}/man1/vala-gen-introspect.1*
%{_mandir}/man1/vala-gen-introspect-%{api_ver}.1*
%{_mandir}/man1/vapigen.1*
%{_mandir}/man1/vapigen-%{api_ver}.1*
%{_mandir}/man1/valadoc-%{api_ver}.1*
%{_mandir}/man1/valadoc.1*
%doc %{_datadir}/devhelp/books/vala-%{api_ver}

%changelog
* Sat Mar 18 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 0.56.3-3
- fix issue : I6O4ZM

* Mon Mar 13 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 0.56.3-2
- remove rpath

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 0.56.3-1
- Upgrade to 0.56.3

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.56.1-1
- Upgrade to 0.56.1

* Tue Apr 19 2022 dillon chen <dillon.chen@gmail.com> - 0.56.0-1
- Update to 0.56.0

* Thu Jun 24 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 0.50.8-3
- Add missing Provides: vala-tools

* Sun May 23 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.50.8-2
- Correct package depends
- Delete unnecessary and uncorrect Requires, Provides, Obsoletes, Conflicts
- Update Release to 2

* Wed May 19 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.50.8-1
- Upgrade to 0.50.8
- Update api_ver, Version, Release, Source0

* Wed Jun 3 2020 wutao <wutao61@huawei.com> - 0.48.6-2
- Upagrade to 0.48.6-2

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.42.2-2
- Package init
